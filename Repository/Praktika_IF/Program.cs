﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Praktika_IF
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Iveskite savo amziu");
            int m = Convert.ToInt32(Console.ReadLine());


            if (m == 17)
            {
                Console.WriteLine("\nTuojaus prasides loterija!");
                Console.ReadLine();
            }

            else if (m == 18)
            {
                Console.WriteLine("\nSveikiname, loterija prasideda");
                Console.ReadLine();
            }

            else if (m > 18 && m < 26)
            {                             
                if (m == 25)
                {
                    Console.WriteLine("\nLoterija tuojaus baigsis");
                    Console.ReadLine();
                }
                else
                {
                    Console.WriteLine("\nJus jau dalyvaujate loterijoje");
                    Console.ReadLine();
                }
            }

            else if (m == 26)
            {
                Console.WriteLine("\nSveikiname loterija baigiasi");
                Console.ReadLine();
            }

            else if (m > 26)
            {
                if (m > 65)
                {
                    Console.WriteLine("\nJus jau pensininkas");
                    Console.ReadLine();
                }
                else
                {
                    Console.WriteLine("\nJus loterijoje nebedalyvaujate");
                    Console.ReadLine();
                }
            }

            else
            {
                Console.WriteLine("\nJus dar per jaunas dalyvauti loterijoje");
                Console.ReadLine();
            }


        }
    }
}
