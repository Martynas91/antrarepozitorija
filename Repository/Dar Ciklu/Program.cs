﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dar_Ciklu
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Parasykite kiek vardu noresite ivesti");
            int x = Convert.ToInt32(Console.ReadLine());

            string[] vardas = new string[x];
            int last = vardas.Length - 1;
            for (int i = 0; i < vardas.Length; i++)
            {
                Console.WriteLine(Environment.NewLine + "Iveskite varda");
                vardas[i] = Console.ReadLine();
            }

            Array.Sort(vardas);
            foreach (string value in vardas)
            {
                if (object.Equals(value, vardas.Last()))
                {
                    Console.Write(value + ".");
                }
                else
                {
                    Console.Write(value + ", ");
                }
            }
            Console.ReadLine();
        }
    }
}
