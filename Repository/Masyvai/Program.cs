﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masyvai
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] vardas = new string[5];

            for (int i = 0; i < vardas.Length; i++)
            {
                Console.WriteLine(Environment.NewLine + "Iveskite varda");
                vardas[i] = Console.ReadLine();
            }

            Array.Sort(vardas);
            for (int i = 0; i < vardas.Length; i++)
            {
                if (i == vardas.Length - 1)
                {
                    Console.Write(vardas[i] + ".");
                }
                else
                {
                    Console.Write(vardas[i] + ", ");
                }
            }

            Console.ReadLine();
        }
    }
}
