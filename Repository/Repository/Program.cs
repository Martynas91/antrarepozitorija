﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Salyga: skaicius yra didesnis uz 45, bet mazesnis uz 69 arba didesnis uz 90");
            Console.WriteLine("\n");
            Console.WriteLine("\nIveskite norima skaiciu");
            int m = Convert.ToInt32(Console.ReadLine());

            if (DaugiauFunkcija(m) && MaziauFunkcija(m) || m > 90)
            {
                Console.WriteLine("\nIvedete teisingai");
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine("\nIvedete neteisingai)");
                Console.ReadLine();
            }

        }

        static bool DaugiauFunkcija(int x)
        {
            return x > 45;
        }

        static bool MaziauFunkcija(int x)
        {
            return x < 69;
        }
    }
}
