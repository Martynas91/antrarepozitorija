﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SecurityLib;

namespace Ciklai2
{
    class Program
    {
        static void Main(string[] args)
        {
            long i = 0;

            while (i < long.MaxValue)
            {
                i++;
                if (SuperSecure.WhatsNumber(i))
                {
                    Console.WriteLine(i);
                    break;
                }        
            }
            Console.WriteLine("\nRadau!");
            Console.ReadLine();
        }
    }
}
