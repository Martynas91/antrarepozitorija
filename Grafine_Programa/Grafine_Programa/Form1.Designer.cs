﻿namespace Grafine_Programa
{
    partial class Mygtukas1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TopTekstas = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.Ekranas = new System.Windows.Forms.Label();
            this.Mygtukas2 = new System.Windows.Forms.Button();
            this.Inputas = new System.Windows.Forms.TextBox();
            this.InputMygtukas = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // TopTekstas
            // 
            this.TopTekstas.AutoSize = true;
            this.TopTekstas.Location = new System.Drawing.Point(156, 9);
            this.TopTekstas.Name = "TopTekstas";
            this.TopTekstas.Size = new System.Drawing.Size(130, 17);
            this.TopTekstas.TabIndex = 0;
            this.TopTekstas.Text = "Cia gali buti tekstas";
            this.TopTekstas.Click += new System.EventHandler(this.label1_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(159, 61);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(89, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "PUSH ME!";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Ekranas
            // 
            this.Ekranas.AutoSize = true;
            this.Ekranas.Location = new System.Drawing.Point(92, 120);
            this.Ekranas.Name = "Ekranas";
            this.Ekranas.Size = new System.Drawing.Size(98, 17);
            this.Ekranas.TabIndex = 2;
            this.Ekranas.Text = "Antras tekstas";
            this.Ekranas.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // Mygtukas2
            // 
            this.Mygtukas2.Location = new System.Drawing.Point(75, 140);
            this.Mygtukas2.Name = "Mygtukas2";
            this.Mygtukas2.Size = new System.Drawing.Size(173, 23);
            this.Mygtukas2.TabIndex = 3;
            this.Mygtukas2.Text = "PUSH ME 2";
            this.Mygtukas2.UseVisualStyleBackColor = true;
            this.Mygtukas2.Click += new System.EventHandler(this.Mygtukas2_Click);
            // 
            // Inputas
            // 
            this.Inputas.Location = new System.Drawing.Point(75, 226);
            this.Inputas.Name = "Inputas";
            this.Inputas.Size = new System.Drawing.Size(195, 22);
            this.Inputas.TabIndex = 4;
            this.Inputas.TextChanged += new System.EventHandler(this.Inputas_TextChanged);
            // 
            // InputMygtukas
            // 
            this.InputMygtukas.Location = new System.Drawing.Point(129, 254);
            this.InputMygtukas.Name = "InputMygtukas";
            this.InputMygtukas.Size = new System.Drawing.Size(75, 23);
            this.InputMygtukas.TabIndex = 5;
            this.InputMygtukas.Text = "button2";
            this.InputMygtukas.UseVisualStyleBackColor = true;
            this.InputMygtukas.Click += new System.EventHandler(this.InputMygtukas_Click);
            // 
            // Mygtukas1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(439, 361);
            this.Controls.Add(this.InputMygtukas);
            this.Controls.Add(this.Inputas);
            this.Controls.Add(this.Mygtukas2);
            this.Controls.Add(this.Ekranas);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.TopTekstas);
            this.Name = "Mygtukas1";
            this.Text = "Grafine Programa";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label TopTekstas;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label Ekranas;
        private System.Windows.Forms.Button Mygtukas2;
        private System.Windows.Forms.TextBox Inputas;
        private System.Windows.Forms.Button InputMygtukas;
    }
}

