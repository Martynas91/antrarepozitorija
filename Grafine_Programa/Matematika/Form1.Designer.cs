﻿namespace Matematika
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.InputBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SkaiciuotiKvadrata = new System.Windows.Forms.Button();
            this.Atsakymas = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // InputBox
            // 
            this.InputBox.Location = new System.Drawing.Point(212, 54);
            this.InputBox.Name = "InputBox";
            this.InputBox.Size = new System.Drawing.Size(200, 22);
            this.InputBox.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(269, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Iveskit skaiciu";
            // 
            // SkaiciuotiKvadrata
            // 
            this.SkaiciuotiKvadrata.Location = new System.Drawing.Point(250, 92);
            this.SkaiciuotiKvadrata.Name = "SkaiciuotiKvadrata";
            this.SkaiciuotiKvadrata.Size = new System.Drawing.Size(140, 46);
            this.SkaiciuotiKvadrata.TabIndex = 2;
            this.SkaiciuotiKvadrata.Text = "Suskaiciuoti kvadrata";
            this.SkaiciuotiKvadrata.UseVisualStyleBackColor = true;
            this.SkaiciuotiKvadrata.Click += new System.EventHandler(this.SkaiciuotiKvadrata_Click);
            // 
            // Atsakymas
            // 
            this.Atsakymas.AutoSize = true;
            this.Atsakymas.Location = new System.Drawing.Point(247, 214);
            this.Atsakymas.Name = "Atsakymas";
            this.Atsakymas.Size = new System.Drawing.Size(126, 17);
            this.Atsakymas.TabIndex = 3;
            this.Atsakymas.Text = "Cia bus atsakymas";
            this.Atsakymas.Click += new System.EventHandler(this.label2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(723, 428);
            this.Controls.Add(this.Atsakymas);
            this.Controls.Add(this.SkaiciuotiKvadrata);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.InputBox);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox InputBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button SkaiciuotiKvadrata;
        private System.Windows.Forms.Label Atsakymas;
    }
}

