﻿namespace Skaiciuotuvas
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Myg0 = new System.Windows.Forms.Button();
            this.Myg1 = new System.Windows.Forms.Button();
            this.Myg2 = new System.Windows.Forms.Button();
            this.Myg3 = new System.Windows.Forms.Button();
            this.Myg4 = new System.Windows.Forms.Button();
            this.Myg5 = new System.Windows.Forms.Button();
            this.Myg6 = new System.Windows.Forms.Button();
            this.Myg7 = new System.Windows.Forms.Button();
            this.Myg8 = new System.Windows.Forms.Button();
            this.Myg9 = new System.Windows.Forms.Button();
            this.MygPlus = new System.Windows.Forms.Button();
            this.MygMinus = new System.Windows.Forms.Button();
            this.MygDaug = new System.Windows.Forms.Button();
            this.MygDalyb = new System.Windows.Forms.Button();
            this.Ekranas = new System.Windows.Forms.TextBox();
            this.MygLygu = new System.Windows.Forms.Button();
            this.MygTaskas = new System.Windows.Forms.Button();
            this.MygCE = new System.Windows.Forms.Button();
            this.MygPlusMinus = new System.Windows.Forms.Button();
            this.MygKvadratu = new System.Windows.Forms.Button();
            this.MygSaknis = new System.Windows.Forms.Button();
            this.MygProc = new System.Windows.Forms.Button();
            this.MygPakeltaBetkokiu = new System.Windows.Forms.Button();
            this.MygDEL = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Myg0
            // 
            this.Myg0.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Myg0.Location = new System.Drawing.Point(175, 329);
            this.Myg0.Name = "Myg0";
            this.Myg0.Size = new System.Drawing.Size(70, 50);
            this.Myg0.TabIndex = 0;
            this.Myg0.Text = "0";
            this.Myg0.UseVisualStyleBackColor = true;
            this.Myg0.Click += new System.EventHandler(this.Myg0_Click);
            // 
            // Myg1
            // 
            this.Myg1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.Myg1.Location = new System.Drawing.Point(99, 273);
            this.Myg1.Name = "Myg1";
            this.Myg1.Size = new System.Drawing.Size(70, 50);
            this.Myg1.TabIndex = 1;
            this.Myg1.Text = "1";
            this.Myg1.UseVisualStyleBackColor = true;
            this.Myg1.Click += new System.EventHandler(this.Myg1_Click);
            // 
            // Myg2
            // 
            this.Myg2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.Myg2.Location = new System.Drawing.Point(175, 273);
            this.Myg2.Name = "Myg2";
            this.Myg2.Size = new System.Drawing.Size(70, 50);
            this.Myg2.TabIndex = 2;
            this.Myg2.Text = "2";
            this.Myg2.UseVisualStyleBackColor = true;
            this.Myg2.Click += new System.EventHandler(this.button3_Click);
            // 
            // Myg3
            // 
            this.Myg3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.Myg3.Location = new System.Drawing.Point(251, 273);
            this.Myg3.Name = "Myg3";
            this.Myg3.Size = new System.Drawing.Size(70, 50);
            this.Myg3.TabIndex = 3;
            this.Myg3.Text = "3";
            this.Myg3.UseVisualStyleBackColor = true;
            this.Myg3.Click += new System.EventHandler(this.Myg3_Click);
            // 
            // Myg4
            // 
            this.Myg4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.Myg4.Location = new System.Drawing.Point(99, 217);
            this.Myg4.Name = "Myg4";
            this.Myg4.Size = new System.Drawing.Size(70, 50);
            this.Myg4.TabIndex = 4;
            this.Myg4.Text = "4";
            this.Myg4.UseVisualStyleBackColor = true;
            this.Myg4.Click += new System.EventHandler(this.Myg4_Click);
            // 
            // Myg5
            // 
            this.Myg5.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.Myg5.Location = new System.Drawing.Point(175, 217);
            this.Myg5.Name = "Myg5";
            this.Myg5.Size = new System.Drawing.Size(70, 50);
            this.Myg5.TabIndex = 5;
            this.Myg5.Text = "5";
            this.Myg5.UseVisualStyleBackColor = true;
            this.Myg5.Click += new System.EventHandler(this.Myg5_Click);
            // 
            // Myg6
            // 
            this.Myg6.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.Myg6.Location = new System.Drawing.Point(251, 217);
            this.Myg6.Name = "Myg6";
            this.Myg6.Size = new System.Drawing.Size(70, 50);
            this.Myg6.TabIndex = 6;
            this.Myg6.Text = "6";
            this.Myg6.UseVisualStyleBackColor = true;
            this.Myg6.Click += new System.EventHandler(this.Myg6_Click);
            // 
            // Myg7
            // 
            this.Myg7.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.Myg7.Location = new System.Drawing.Point(99, 161);
            this.Myg7.Name = "Myg7";
            this.Myg7.Size = new System.Drawing.Size(70, 50);
            this.Myg7.TabIndex = 7;
            this.Myg7.Text = "7";
            this.Myg7.UseVisualStyleBackColor = true;
            this.Myg7.Click += new System.EventHandler(this.Myg7_Click);
            // 
            // Myg8
            // 
            this.Myg8.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.Myg8.Location = new System.Drawing.Point(175, 161);
            this.Myg8.Name = "Myg8";
            this.Myg8.Size = new System.Drawing.Size(70, 50);
            this.Myg8.TabIndex = 8;
            this.Myg8.Text = "8";
            this.Myg8.UseVisualStyleBackColor = true;
            this.Myg8.Click += new System.EventHandler(this.Myg8_Click);
            // 
            // Myg9
            // 
            this.Myg9.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.Myg9.Location = new System.Drawing.Point(251, 161);
            this.Myg9.Name = "Myg9";
            this.Myg9.Size = new System.Drawing.Size(70, 50);
            this.Myg9.TabIndex = 9;
            this.Myg9.Text = "9";
            this.Myg9.UseVisualStyleBackColor = true;
            this.Myg9.Click += new System.EventHandler(this.button10_Click);
            // 
            // MygPlus
            // 
            this.MygPlus.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.MygPlus.Location = new System.Drawing.Point(327, 329);
            this.MygPlus.Name = "MygPlus";
            this.MygPlus.Size = new System.Drawing.Size(70, 50);
            this.MygPlus.TabIndex = 10;
            this.MygPlus.Text = "+";
            this.MygPlus.UseVisualStyleBackColor = true;
            this.MygPlus.Click += new System.EventHandler(this.MygPlus_Click);
            // 
            // MygMinus
            // 
            this.MygMinus.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.MygMinus.Location = new System.Drawing.Point(327, 273);
            this.MygMinus.Name = "MygMinus";
            this.MygMinus.Size = new System.Drawing.Size(70, 50);
            this.MygMinus.TabIndex = 11;
            this.MygMinus.Text = "-";
            this.MygMinus.UseVisualStyleBackColor = true;
            this.MygMinus.Click += new System.EventHandler(this.MygMinus_Click);
            // 
            // MygDaug
            // 
            this.MygDaug.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.MygDaug.Location = new System.Drawing.Point(327, 217);
            this.MygDaug.Name = "MygDaug";
            this.MygDaug.Size = new System.Drawing.Size(70, 50);
            this.MygDaug.TabIndex = 12;
            this.MygDaug.Text = "*";
            this.MygDaug.UseVisualStyleBackColor = true;
            this.MygDaug.Click += new System.EventHandler(this.MygDaug_Click);
            // 
            // MygDalyb
            // 
            this.MygDalyb.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.MygDalyb.Location = new System.Drawing.Point(327, 161);
            this.MygDalyb.Name = "MygDalyb";
            this.MygDalyb.Size = new System.Drawing.Size(70, 50);
            this.MygDalyb.TabIndex = 13;
            this.MygDalyb.Text = "/";
            this.MygDalyb.UseVisualStyleBackColor = true;
            this.MygDalyb.Click += new System.EventHandler(this.MygDalyb_Click);
            // 
            // Ekranas
            // 
            this.Ekranas.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.Ekranas.Location = new System.Drawing.Point(99, 39);
            this.Ekranas.Multiline = true;
            this.Ekranas.Name = "Ekranas";
            this.Ekranas.Size = new System.Drawing.Size(397, 50);
            this.Ekranas.TabIndex = 14;
            this.Ekranas.TextChanged += new System.EventHandler(this.Ekranas_TextChanged);
            // 
            // MygLygu
            // 
            this.MygLygu.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.MygLygu.Location = new System.Drawing.Point(327, 385);
            this.MygLygu.Name = "MygLygu";
            this.MygLygu.Size = new System.Drawing.Size(70, 50);
            this.MygLygu.TabIndex = 15;
            this.MygLygu.Text = "=";
            this.MygLygu.UseVisualStyleBackColor = true;
            this.MygLygu.Click += new System.EventHandler(this.MygLygu_Click);
            // 
            // MygTaskas
            // 
            this.MygTaskas.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.MygTaskas.Location = new System.Drawing.Point(251, 329);
            this.MygTaskas.Name = "MygTaskas";
            this.MygTaskas.Size = new System.Drawing.Size(70, 50);
            this.MygTaskas.TabIndex = 16;
            this.MygTaskas.Text = ".";
            this.MygTaskas.UseVisualStyleBackColor = true;
            this.MygTaskas.Click += new System.EventHandler(this.MygTaskas_Click);
            // 
            // MygCE
            // 
            this.MygCE.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.MygCE.Location = new System.Drawing.Point(99, 105);
            this.MygCE.Name = "MygCE";
            this.MygCE.Size = new System.Drawing.Size(70, 50);
            this.MygCE.TabIndex = 17;
            this.MygCE.Text = "CE";
            this.MygCE.UseVisualStyleBackColor = true;
            this.MygCE.Click += new System.EventHandler(this.MygCE_Click);
            // 
            // MygPlusMinus
            // 
            this.MygPlusMinus.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.MygPlusMinus.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.MygPlusMinus.Location = new System.Drawing.Point(99, 329);
            this.MygPlusMinus.Name = "MygPlusMinus";
            this.MygPlusMinus.Size = new System.Drawing.Size(70, 50);
            this.MygPlusMinus.TabIndex = 18;
            this.MygPlusMinus.Text = "+/-";
            this.MygPlusMinus.UseVisualStyleBackColor = true;
            this.MygPlusMinus.Click += new System.EventHandler(this.MygPlusMinus_Click);
            // 
            // MygKvadratu
            // 
            this.MygKvadratu.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.MygKvadratu.Location = new System.Drawing.Point(426, 273);
            this.MygKvadratu.Name = "MygKvadratu";
            this.MygKvadratu.Size = new System.Drawing.Size(70, 50);
            this.MygKvadratu.TabIndex = 19;
            this.MygKvadratu.Text = "^2";
            this.MygKvadratu.UseVisualStyleBackColor = true;
            this.MygKvadratu.Click += new System.EventHandler(this.MygKvadratu_Click);
            // 
            // MygSaknis
            // 
            this.MygSaknis.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.MygSaknis.Location = new System.Drawing.Point(426, 217);
            this.MygSaknis.Name = "MygSaknis";
            this.MygSaknis.Size = new System.Drawing.Size(70, 50);
            this.MygSaknis.TabIndex = 20;
            this.MygSaknis.Text = "SQRT";
            this.MygSaknis.UseVisualStyleBackColor = true;
            this.MygSaknis.Click += new System.EventHandler(this.MygSaknis_Click);
            // 
            // MygProc
            // 
            this.MygProc.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.MygProc.Location = new System.Drawing.Point(426, 161);
            this.MygProc.Name = "MygProc";
            this.MygProc.Size = new System.Drawing.Size(70, 50);
            this.MygProc.TabIndex = 21;
            this.MygProc.Text = "%";
            this.MygProc.UseVisualStyleBackColor = true;
            this.MygProc.Click += new System.EventHandler(this.button1_Click);
            // 
            // MygPakeltaBetkokiu
            // 
            this.MygPakeltaBetkokiu.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.MygPakeltaBetkokiu.Location = new System.Drawing.Point(426, 329);
            this.MygPakeltaBetkokiu.Name = "MygPakeltaBetkokiu";
            this.MygPakeltaBetkokiu.Size = new System.Drawing.Size(70, 50);
            this.MygPakeltaBetkokiu.TabIndex = 22;
            this.MygPakeltaBetkokiu.Text = "^x";
            this.MygPakeltaBetkokiu.UseVisualStyleBackColor = true;
            this.MygPakeltaBetkokiu.Click += new System.EventHandler(this.MygPakeltaBetkokiu_Click);
            // 
            // MygDEL
            // 
            this.MygDEL.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.MygDEL.Location = new System.Drawing.Point(175, 105);
            this.MygDEL.Name = "MygDEL";
            this.MygDEL.Size = new System.Drawing.Size(70, 50);
            this.MygDEL.TabIndex = 23;
            this.MygDEL.Text = "DEL";
            this.MygDEL.UseVisualStyleBackColor = true;
            this.MygDEL.Click += new System.EventHandler(this.MygDEL_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(580, 481);
            this.Controls.Add(this.MygDEL);
            this.Controls.Add(this.MygPakeltaBetkokiu);
            this.Controls.Add(this.MygProc);
            this.Controls.Add(this.MygSaknis);
            this.Controls.Add(this.MygKvadratu);
            this.Controls.Add(this.MygPlusMinus);
            this.Controls.Add(this.MygCE);
            this.Controls.Add(this.MygTaskas);
            this.Controls.Add(this.MygLygu);
            this.Controls.Add(this.Ekranas);
            this.Controls.Add(this.MygDalyb);
            this.Controls.Add(this.MygDaug);
            this.Controls.Add(this.MygMinus);
            this.Controls.Add(this.MygPlus);
            this.Controls.Add(this.Myg9);
            this.Controls.Add(this.Myg8);
            this.Controls.Add(this.Myg7);
            this.Controls.Add(this.Myg6);
            this.Controls.Add(this.Myg5);
            this.Controls.Add(this.Myg4);
            this.Controls.Add(this.Myg3);
            this.Controls.Add(this.Myg2);
            this.Controls.Add(this.Myg1);
            this.Controls.Add(this.Myg0);
            this.Name = "Form1";
            this.Text = "SKAICIUOTUVAS";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Myg0;
        private System.Windows.Forms.Button Myg1;
        private System.Windows.Forms.Button Myg2;
        private System.Windows.Forms.Button Myg3;
        private System.Windows.Forms.Button Myg4;
        private System.Windows.Forms.Button Myg5;
        private System.Windows.Forms.Button Myg6;
        private System.Windows.Forms.Button Myg7;
        private System.Windows.Forms.Button Myg8;
        private System.Windows.Forms.Button Myg9;
        private System.Windows.Forms.Button MygPlus;
        private System.Windows.Forms.Button MygMinus;
        private System.Windows.Forms.Button MygDaug;
        private System.Windows.Forms.Button MygDalyb;
        private System.Windows.Forms.TextBox Ekranas;
        private System.Windows.Forms.Button MygLygu;
        private System.Windows.Forms.Button MygTaskas;
        private System.Windows.Forms.Button MygCE;
        private System.Windows.Forms.Button MygPlusMinus;
        private System.Windows.Forms.Button MygKvadratu;
        private System.Windows.Forms.Button MygSaknis;
        private System.Windows.Forms.Button MygProc;
        private System.Windows.Forms.Button MygPakeltaBetkokiu;
        private System.Windows.Forms.Button MygDEL;
    }
}

