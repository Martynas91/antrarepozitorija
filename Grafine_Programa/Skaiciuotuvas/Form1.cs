﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Skaiciuotuvas
{
    public partial class Form1 : Form
    {
        string sk1 = "";
        string zenklas;
           
        public Form1()
        {
            InitializeComponent();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            RasykIEkrana("9");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            RasykIEkrana("2");
        }

        private void Myg0_Click(object sender, EventArgs e)
        {
            if (!(Ekranas.Text.Length == 1 && Ekranas.Text[0]=='0'))
            {
                RasykIEkrana("0");
            }
            
        }

        private void Myg1_Click(object sender, EventArgs e)
        {
            RasykIEkrana("1");
        }

        private void Myg3_Click(object sender, EventArgs e)
        {
            RasykIEkrana("3");
        }

        private void Myg4_Click(object sender, EventArgs e)
        {
            RasykIEkrana("4");
        }

        private void Myg5_Click(object sender, EventArgs e)
        {
            RasykIEkrana("5");
        }

        private void Myg6_Click(object sender, EventArgs e)
        {
            RasykIEkrana("6");
        }

        private void Myg7_Click(object sender, EventArgs e)
        {
            RasykIEkrana("7");
        }

        private void Myg8_Click(object sender, EventArgs e)
        {
            RasykIEkrana("8");
        }

        private void MygTaskas_Click(object sender, EventArgs e)
        {
            if (Ekranas.Text.Length == 0)
            {
                RasykIEkrana("0.");
            }
            else if (Ekranas.Text.Contains("."))
            {
                return;
            }

            else
            {
                RasykIEkrana(".");
            }
            
        }

        private void MygPlus_Click(object sender, EventArgs e)
        {
            sk1 = Ekranas.Text;
            Ekranas.Text = "";
            zenklas = "+";
            
        }

        private void MygMinus_Click(object sender, EventArgs e)
        {
            sk1 = Ekranas.Text;
            Ekranas.Text = "";
            zenklas = "-";
        }

        private void MygDaug_Click(object sender, EventArgs e)
        {
            sk1 = Ekranas.Text;
            Ekranas.Text = "";
            zenklas = "*";
        }

        private void MygDalyb_Click(object sender, EventArgs e)
        {
            sk1 = Ekranas.Text;
            Ekranas.Text = "";
            zenklas = "/";
        }

        private void MygCE_Click(object sender, EventArgs e)
        {
            Ekranas.Text = "";
        }

        private void Ekranas_TextChanged(object sender, EventArgs e)
        {

        }

        private void RasykIEkrana(string num1)
        {
            if (Ekranas.Text.Length == 1 && Ekranas.Text[0] == '0')
            {
                Ekranas.Text = "";
            }

            if (Ekranas.TextLength < 8)
            {
                Ekranas.Text = Ekranas.Text + num1;
            }
        }

        private void MygLygu_Click(object sender, EventArgs e)
        {
            if (sk1.Length == 0 || Ekranas.Text.Length == 0)
            {
                return;
            }

                double num1 = Convert.ToDouble(sk1);
                double num2 = Convert.ToDouble(Ekranas.Text);
                double sum = 0;

                if (zenklas == "+")
                {
                    sum = num1 + num2;
                }

                else if (zenklas == "-")
                {
                    sum = num1 - num2;
                }
                else if (zenklas == "*")
                {
                    sum = num1 * num2;
                }
                else if (zenklas == "/")
                {
                    if (num2 == 0)
                {
                    MessageBox.Show("Dalyba is 0 negalima");
                }
                else
                {
                    sum = num1 / num2;
                }

                }
                else if (zenklas == "^")
                {
                    sum = Math.Pow(num1, num2);
                }

               // else if (zenklas == "%")
            {
                sum = num1 * num2 / 100;
            }

                Ekranas.Text = sum.ToString();
        }

        private void MygPlusMinus_Click(object sender, EventArgs e)
        {
            int sk = Convert.ToInt32(Ekranas.Text);
            int zenklas = -1;
            Ekranas.Text = Convert.ToString(sk * zenklas);
            
        }

        private void MygKvadratu_Click(object sender, EventArgs e)
        {
            double num1 = Convert.ToDouble(Ekranas.Text);
            double sum = Math.Pow(num1, 2);
            Ekranas.Text = Convert.ToString(sum);

        }

        private void MygSaknis_Click(object sender, EventArgs e)
        {
            double num1 = Convert.ToDouble(Ekranas.Text);
            double sum = Math.Sqrt(num1);
            Ekranas.Text = Convert.ToString(sum);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            sk1 = Ekranas.Text;
            Ekranas.Text = "";
            zenklas = "%";
        }

        private void MygPakeltaBetkokiu_Click(object sender, EventArgs e)
        {
            sk1 = Ekranas.Text;
            Ekranas.Text = "";
            zenklas = "^";
        }

        private void MygDEL_Click(object sender, EventArgs e)
        {
            string Ekr = Ekranas.Text;

            if (Ekr.Length > 0)
            {
                Ekr = Ekr.Substring(0, Ekr.Length - 1);
            }
            Ekranas.Text = Ekr;
        }
    }
}
